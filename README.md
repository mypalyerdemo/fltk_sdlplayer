# fltk_sdlplayer

#### 介绍
使用fltk，sdl2，ffms2编写的播放器
ffms2对ffmpeg进行了封装，比直接调用ffmpeg方便了许多
sdl2常用的音视频库
fltk一个跨平的窗口库，比gtk要简单多
soundtouch音频处理工具实现变声及变速

#### 使用说明
使用NetBeans IDE工具编写
编译环境为msys2中的mingw32
文件夹opencv_demo为单独一个工程，用来参考opencv

pacman -S mingw-w64-$arch-x86_64-fltk
pacman -S mingw-w64-$arch-x86_64-SDL2_image
pacman -S mingw-w64-$arch-x86_64-ffms2
pacman -S mingw-w64-$arch-x86_64-opencv
pacman -S mingw-w64-$arch-x86_64-soundtouch
pacman -S mingw-w64-$arch-x86_64-faac
#### 已实现功能

1.  播放视频文件
2.  读取图片并显示
3.  保存视频截图
4.  增加声音录制

#### 待完善功能

1.  音视频同步
2.  声音变声变速
3.  边录音边播放

#### 更新日志
2022.02.14
桌面录制增加声音录制，float类型，faac编码

2022.02.03
增加x264使用RGB编码，减少一次yuv的转换

2022.02.02
增加了屏幕录制使用x264编码保存为flv文件
旧的东西，重新写了下
现在x264支持RGB编码，不用再先转换I420了

2022.01.26
目录调整
增加截屏

2021.06.20
处理播放页面被关时还显示问题
从主窗口分离图片读取
图片读取保持比例

2021.06.18
播放器展示更换方式，不使用SDL画界面

2021.03.31
利用soundTouch进行变速和变调，待调整

2021.03.26
录音机变速功能开始编写

2021.03.05
录音机跟播放器进行分离

2021.02.27
使用SDL进行录音机制作

2021.02.21
利用ffms制作播放器雏形


#### 待添加功能(可能会做吧)
1.  人脸识别
2.  车牌识别
3.  音视频捕获

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
