/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   videoencoder.h
 * Author: Administrator
 *
 * Created on 2022年1月27日, 上午10:53
 */

#ifndef VIDEOENCODER_H
#define VIDEOENCODER_H
#include<stdint.h>
#include<stdio.h>
//就这个公开对外
//也就x264
typedef enum _COLORTYPE{
COLORTYPE_I420,
COLORTYPE_RGB24
}COLORTYPE;
class videoencoder {
public:
	videoencoder();
	videoencoder(const videoencoder& orig);
	virtual ~videoencoder();
	//根据文件类型创建编码器
	static videoencoder*create_videoencoder_bytype(const char*type);
	virtual int init(int width,int height, int fps,COLORTYPE colortype)=0;
	virtual int close()=0;
	virtual int get_x264_headinfo(uint8_t*sps, int*spslen, uint8_t*pps, int*ppslen)=0;
	virtual uint8_t*encoder_x264_frame(uint8_t*indata, int inlen, int*outlen)=0;
private:

};

#endif /* VIDEOENCODER_H */

