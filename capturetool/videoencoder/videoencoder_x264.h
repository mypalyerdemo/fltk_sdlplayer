/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   videoencoder_x264.h
 * Author: Administrator
 *
 * Created on 2022年1月27日, 上午11:20
 */

#ifndef VIDEOENCODER_X264_H
#define VIDEOENCODER_X264_H
#include "videoencoder.h"
#include <x264.h>
class videoencoder_x264:public  videoencoder{
	x264_t* m_hHandle;
	x264_picture_t m_pic;
	int m_width;
	int m_height;
	COLORTYPE m_colortype;
public:
	videoencoder_x264();
	videoencoder_x264(const videoencoder_x264& orig);
	virtual ~videoencoder_x264();
	int init(int width,int height, int fps,COLORTYPE colortype);
	int close();
	int get_x264_headinfo(uint8_t*sps, int*spslen, uint8_t*pps, int*ppslen);
	uint8_t*encoder_x264_frame(uint8_t*indata, int inlen, int*outlen);
private:

};

#endif /* VIDEOENCODER_X264_H */

