/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   videoencoder_x264.cpp
 * Author: Administrator
 * 
 * Created on 2022年1月27日, 上午11:20
 */

#include "videoencoder_x264.h"
#include <string.h>
videoencoder_x264::videoencoder_x264()
{
}

videoencoder_x264::videoencoder_x264(const videoencoder_x264& orig)
{
}

videoencoder_x264::~videoencoder_x264()
{
}

int videoencoder_x264::init(int width,int height, int fps,COLORTYPE colortype){
	x264_param_t param;
	x264_param_default(&param);
	x264_param_default_preset(&param,"fast","zerolatency");
	param.i_width = width;
	param.i_height = height;
	//param.i_log_level = X264_LOG_NONE;

	param.i_fps_num = fps;
	param.i_keyint_min = 25;
	param.i_keyint_max = 50;
//每个关键帧前都输出sps,pps信息,1是
	param.b_repeat_headers = 0;
//0前4个字节为改帧长度，1分割单元，3个0或4个0
	param.b_annexb = 0;
	if(colortype==COLORTYPE_RGB24){
		param.i_csp=X264_CSP_BGR;
	}
	//param.b_annexb = 1;
	m_hHandle = x264_encoder_open(&param);
	x264_picture_init(&m_pic);
//支持RGB了		
	if(colortype==COLORTYPE_RGB24){
		//X264_CSP_VFLIP
		x264_picture_alloc(&m_pic,X264_CSP_BGR|X264_CSP_VFLIP,width,height);
	}else{
		x264_picture_alloc(&m_pic,X264_CSP_I420,width,height);
	}
	m_width=width;
	m_height=height;
	m_colortype=colortype;
	return 0;
}
int videoencoder_x264::close(){
	x264_encoder_close(m_hHandle);
	x264_picture_clean(&m_pic);
	return 0;
}
int videoencoder_x264::get_x264_headinfo(uint8_t*sps, int*spslen, uint8_t*pps, int*ppslen){
	int pi_nal = 0;
	x264_nal_t*pp_nal = NULL;
	x264_encoder_headers(m_hHandle, &pp_nal, &pi_nal);
	*spslen = pp_nal[0].i_payload - 4;
	memcpy(sps, pp_nal[0].p_payload + 4, *spslen);
	*ppslen = pp_nal[1].i_payload - 4;
	memcpy(pps, pp_nal[1].p_payload + 4, *ppslen);	
	return 0;
}
uint8_t*videoencoder_x264::encoder_x264_frame(uint8_t*indata, int inlen, int*outlen){
	x264_picture_t picout;
	int i_nal = 0;
	x264_nal_t*nal = NULL;
	if(inlen>0){
		//memcpy(m_pic.img.plane[0], indata, inlen);
		if(m_colortype==COLORTYPE_RGB24){
			memcpy(m_pic.img.plane[0], indata, inlen);
		}else{
			memcpy(m_pic.img.plane[0], indata, m_width*m_height);
			memcpy(m_pic.img.plane[1], indata + m_width*m_height, m_width*m_height / 4);
			memcpy(m_pic.img.plane[2], indata + m_width*m_height / 4 * 5, m_width*m_height / 4);
		}
		*outlen = x264_encoder_encode(m_hHandle, &nal, &i_nal, &m_pic, &picout);
	}else{
		*outlen = x264_encoder_encode(m_hHandle, &nal, &i_nal, NULL, &picout);
	}
	if (*outlen > 0) {
		return nal[0].p_payload;
	}
	return NULL;
}