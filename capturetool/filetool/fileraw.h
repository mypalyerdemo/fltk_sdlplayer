/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fileraw.h
 * Author: zhujia
 *
 * Created on 2022年2月3日, 上午8:45
 */

#ifndef FILERAW_H
#define FILERAW_H
#include<stdint.h>
#include<stdio.h>
class fileraw {
	FILE*m_fp;
public:
	fileraw();
	fileraw(const fileraw& orig);
	virtual ~fileraw();
	
	int create(const char*filename);
	int open(const char*filename);
	int close();
	
	int write_data(const void*data, int len);
	
	int write_datau8(uint32_t u8);
	int write_datau16(uint32_t u16);
	int write_datau24(uint32_t u24);
	int write_datau32(uint32_t u32);
	int write_datautime(uint32_t utime);
private:
	int write_datau(uint32_t data,int len);
};

#endif /* FILTRAW_H */

