#ifndef FILE_WAV_H
#define FILE_WAV_H
//一些用于读写WAV文件的API
#include<stdio.h>
#include<stdint.h>
typedef struct _WavBuffer{
char* wav_buffer;//数据缓存
int   wav_length;//数据缓存大小
char* wav_pos;//播放位置
int   wav_lastlength;//剩余数据大小
}WavBuffer;

typedef struct _WAVE_FORMAT{
uint16_t	wFormatTag;//格式一般PCM,1
uint16_t	nChannels;//声道数mono 1 单声道 stereo 2 双声道
uint32_t	nSamplesPerSec;//采样率(每秒采样数)常见11.025kHz,22.05kHz,44.1kHz,48kHz,96kHz
uint32_t    nAvgBytesPerSec;//平均每秒字节数 声道数*采样率*采样位数/8
uint16_t    nBlockAlign;//对齐字节 声道数*采样位数/8 (单声道8bit 1Byte 双声道16bit 4Byte)
uint16_t    wBitsPerSample;//采样位数8bit 16bit
}WAVE_FORMAT;

typedef struct _FILE_WAV{
WAVE_FORMAT format;//PCM格式
FILE*	 fp;//文件指针
uint32_t total;//PCM数据总长度
uint32_t pos;//当前指针位置
uint8_t  readmode;//1读取模式，0写模式	
}FILE_WAV;
//0成功-1失败
int OpenWavFile(const char*filename,FILE_WAV*fileinfo,int bReadMode);
//返回读取的字节数
int ReadWavFile(FILE_WAV*fileinfo,char*buffer,uint32_t length);
//返回写入的字节数
int WriteWavFile(FILE_WAV*fileinfo,char*buffer,uint32_t length);
int CloseWavFile(FILE_WAV*fileinfo);
//显示音频信息
void ShowWavInfo(WAVE_FORMAT format,uint32_t total);
#endif