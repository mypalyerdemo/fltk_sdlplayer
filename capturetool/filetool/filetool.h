/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   filetool.h
 * Author: Administrator
 *
 * Created on 2022年1月27日, 上午10:56
 */

#ifndef FILETOOL_H
#define FILETOOL_H

#include<stdint.h>
#include<stdio.h>
class filetool {
public:
	filetool();
	filetool(const filetool& orig);
	virtual ~filetool();
	static filetool*create_filetool_bytype(const char*type);
	virtual int create(const char*filename)=0;
	virtual int open(const char*filename)=0;
	virtual int close()=0;
	virtual int write_x264_head(uint8_t*sps, int spslen, uint8_t*pps, int ppslen, int width, int height)=0;
//前4字节为长度	
	virtual int write_x264_frame(uint8_t*data, int len, long timestamp)=0;
	virtual int write_faac_head(uint8_t*data, int len)=0;
	virtual int write_faac_frame(uint8_t*data, int len, long timestamp)=0;
private:

};

#endif /* FILETOOL_H */

