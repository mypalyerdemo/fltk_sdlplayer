/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fileraw.cpp
 * Author: zhujia
 * 
 * Created on 2022年2月3日, 上午8:45
 */

#include "fileraw.h"
#define HTON16(x)  (((x)>>8&0xff)|((x)<<8&0xff00))
#define HTON24(x)  (((x)>>16&0xff)|((x)<<16&0xff0000)|(x)&0xff00)
#define HTON32(x)  (((x)>>24&0xff)|((x)>>8&0xff00)|((x)<<8&0xff0000)|((x)<<24&0xff000000))
#define HTONTIME(x) (((x)>>16&0xff)|((x)<<16&0xff0000)|(x)&0xff00ff00)
#define MAKETYPE(a,b,c,d) (((a)&0xff)|((b)<<8&0xff00)|((c)<<16&0xff0000)|((d)<<24&0xff000000))
fileraw::fileraw() {
}

fileraw::fileraw(const fileraw& orig) {
}

fileraw::~fileraw() {
}
int fileraw::create(const char*filename){
	m_fp=fopen(filename, "wb");
	return 0;
}

int fileraw::open(const char*filename){
	return 0;
}

int fileraw::close(){
	fclose(m_fp);
	return 0;
}
int fileraw::write_data(const void*data, int len){
	return fwrite(data, 1, len, m_fp);
}
int fileraw::write_datau(uint32_t data,int len){
	return fwrite(&data, 1, len, m_fp);
}
int fileraw::write_datau8(uint32_t u8){
	return write_datau(u8,1);
}
int fileraw::write_datau16(uint32_t u16){
	return write_datau(HTON16(u16),2);
}
int fileraw::write_datau24(uint32_t u24){
	return write_datau(HTON24(u24),3);
}
int fileraw::write_datau32(uint32_t u32){
	return write_datau(HTON32(u32),4);
}
int fileraw::write_datautime(uint32_t utime){
	return write_datau(HTONTIME(utime),4);
}