/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   filetool_flv.h
 * Author: Administrator
 *
 * Created on 2022年1月27日, 上午11:23
 */

#ifndef FILETOOL_FLV_H
#define FILETOOL_FLV_H
#include "filetool.h"
#include "fileraw.h"
class filetool_flv:public filetool {
	fileraw*m_file;
public:
	filetool_flv();
	filetool_flv(const filetool_flv& orig);
	virtual ~filetool_flv();
	int create(const char*filename);
	int open(const char*filename);
	int close();
	int write_x264_head(uint8_t*sps, int spslen, uint8_t*pps, int ppslen, int width, int height);
	int write_x264_frame(uint8_t*data, int len, long timestamp);
	int write_faac_head(uint8_t*data, int len);
	int write_faac_frame(uint8_t*data, int len, long timestamp);
private:
	
};

#endif /* FILETOOL_FLV_H */

