/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   filetool_flv.cpp
 * Author: Administrator
 * 
 * Created on 2022年1月27日, 上午11:23
 */

#include "filetool_flv.h"
#include<string.h>
#define FLV_FLAG_VIDEO 0x01
#define FLV_FLAG_AUDIO 0x04
#define FLV_TYPE_VIDEO		9
#define FLV_TYPE_AUDIO		8
#define FLV_TYPE_SCRIPTDATA	18
filetool_flv::filetool_flv()
{
}

filetool_flv::filetool_flv(const filetool_flv& orig)
{
}

filetool_flv::~filetool_flv()
{
}
int filetool_flv::create(const char*filename){
	m_file=new fileraw();
	m_file->create(filename);
	const char*HeadFlag = "FLV";
	m_file->write_data(HeadFlag, 3);
	m_file->write_datau8(1);
	m_file->write_datau8(FLV_FLAG_VIDEO|FLV_FLAG_AUDIO);
	m_file->write_datau32(9);
	m_file->write_datau32(0);
	return 0;
}
int filetool_flv::open(const char*filename){
	return 0;
}
int filetool_flv::close(){
	m_file->close();
	m_file=NULL;
	return 0;
}
int filetool_flv::write_x264_head(uint8_t*sps, int spslen, uint8_t*pps, int ppslen, int width, int height){
	m_file->write_datau8(9);
	m_file->write_datau24(spslen + ppslen + 16);
	m_file->write_datautime(0);
	m_file->write_datau24(0);
///////////////////////////////
	m_file->write_datau8(0x17);
	m_file->write_datau8(0);
	m_file->write_datau24(0);

	m_file->write_datau8(0x01);

	m_file->write_datau8(sps[1]);
	m_file->write_datau8(sps[2]);
	m_file->write_datau8(sps[3]);

	m_file->write_datau8(0x03);
	m_file->write_datau8(0xe1);

	m_file->write_datau16(spslen);
	m_file->write_data(sps, spslen);
	m_file->write_datau8(0x01);
	m_file->write_datau16(ppslen);
	m_file->write_data(pps,ppslen);
///////////////////////////////
	m_file->write_datau32(spslen + ppslen + 16+11);
	return 0;
}
int filetool_flv::write_x264_frame(uint8_t*data, int len, long timestamp){
	m_file->write_datau8(9);
	m_file->write_datau24(len+9-4);
	m_file->write_datautime(timestamp);
	m_file->write_datau24(0);
	///////////////////////////////
	if (data[4] == 0x65) {
		m_file->write_datau8(0x17);
	}	else {
		m_file->write_datau8(0x27);
	}
	m_file->write_datau8(1);
	m_file->write_datau24(0);
	m_file->write_data(data, len);
	///////////////////////////////
	m_file->write_datau32(len + 9 - 4 + 11);
	return 0;
}
typedef struct _ADSTHEADINFO{
uint8_t  is_mp2;
uint8_t  profile;
uint8_t  sr_idx;
uint8_t  nb_ch;
uint16_t frame_size;
}ADSTHEADINFO;
typedef struct _FLVAACADST{
	unsigned char SamplIndex1:3;
	unsigned char OBjecttype:5;//2

	unsigned char other:3;//000
	unsigned char channel:4;
	unsigned char SamplIndex2:1;
}FLVAACADST;
int filetool_flv::write_faac_head(uint8_t*data, int len){
	uint8_t buff[2]={0};
	uint32_t datalen=2+2;
	ADSTHEADINFO*Info=(ADSTHEADINFO*)data;
	buff[0]=(((Info->profile+1)<<3)&0xF8)|((Info->sr_idx&0x0E)>>1);
	buff[1]=((Info->sr_idx<<7)&0x80)|((Info->nb_ch<<3)&0x78);
	int SampIndex=4;
	FLVAACADST m_flvadst={0};
	m_flvadst.OBjecttype=1;
	m_flvadst.SamplIndex1=SampIndex>>1&0x03;
	m_flvadst.SamplIndex2=SampIndex&0x01;	
	m_flvadst.channel=2;
	
	memcpy(buff,&m_flvadst,2);
	
	m_file->write_datau8(8);
	m_file->write_datau24(datalen);
	m_file->write_datautime(0);
	m_file->write_datau24(0);
	
	m_file->write_datau8(0xAF);
	m_file->write_datau8(0x00);
	
	m_file->write_data(buff,2);
	m_file->write_datau32(datalen+11);
	return 0;
}
int filetool_flv::write_faac_frame(uint8_t*data, int len, long timestamp){
	uint32_t datalen=len+2;
	m_file->write_datau8(8);
	m_file->write_datau24(datalen);
	m_file->write_datautime(timestamp);
	m_file->write_datau24(0);
	
	m_file->write_datau8(0xAF);
	m_file->write_datau8(0x01);

	m_file->write_data(data,len);
	m_file->write_datau32(datalen+11);
	return 0;
}