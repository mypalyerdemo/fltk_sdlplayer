/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   bmptool.h
 * Author: Administrator
 *
 * Created on 2022年1月26日, 下午5:08
 */

#ifndef BMPTOOL_H
#define BMPTOOL_H
#include<stdint.h>
//保存桌面图片
void SaveDesktopWindowBmp();
void SaveBmpFileByRGBData(int width, int height,int bits, uint8_t*data);
//获取每行实际宽带
int GetBMPWidthBytes(int Width,int Bits);
void InitDesktoptopBmp24BitData(int width, int height);
void CleanDesktopBmp24BitData();
void GetDeskBmp24BitDataEx(uint8_t*data, int width, int height);
//flip=0
void RGB24toI420(int inWidth, int inHeight, uint8_t *bmp, uint8_t*yuv_out, int flip);
#endif /* BMPTOOL_H */

