/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   capturetool.h
 * Author: Administrator
 *
 * Created on 2022年1月26日, 下午5:31
 */

#ifndef CAPTURETOOL_H
#define CAPTURETOOL_H

#ifndef USE_SDL2
#define USE_SDL2 1
#endif
#include"filetool.h"
#include"videoencoder.h"
#include"audioencoder.h"
#include"wavapi.h"
#define USE_YUV_I420 0
class capturetool{
	uint64_t m_starttime;
	int m_timerid;
	uint32_t m_audiodev;
	filetool*m_filetool;
	FILE_WAV m_wav;
	videoencoder*m_videoencoder;
	audioencoder*m_audioencoder;
	uint8_t*m_data;
	int m_datalen;
#if USE_YUV_I420	
	int m_yuvdatalen;
	uint8_t*m_yuvdata;
#endif	
	int m_height;
	int m_width;
	int m_fps;
	int m_frames;
	uint64_t m_samples;
public:
	capturetool();
	~capturetool();
	int start_recoder();
	int stop_recoder();
	int capture_one_frame();
	int capture_audio(uint8_t* stream,int len);
};

#endif /* CAPTURETOOL_H */

