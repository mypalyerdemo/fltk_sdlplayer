/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   audioencoder_faac.h
 * Author: zhujia
 *
 * Created on 2022年2月6日, 下午8:19
 */

#ifndef AUDIOENCODER_FAAC_H
#define AUDIOENCODER_FAAC_H
#include "audioencoder.h"
#include <faac.h>
class audioencoder_faac:public audioencoder {
	faacEncHandle m_hEncoder;
	uint8_t *m_outputBuffer;
	int m_bufferSize;
	FILE*m_fp;
public:
	audioencoder_faac();
	audioencoder_faac(const audioencoder_faac& orig);
	virtual ~audioencoder_faac();
	int init(int freq,int channels);
	int close();
	int get_faac_headinfo(uint8_t*data, int*datalen);
	uint8_t*encoder_faac_frame(uint8_t*indata, int inlen, int*outlen);
private:

};

#endif /* AUDIOENCODER_FAAC_H */

