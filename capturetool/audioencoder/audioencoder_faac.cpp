/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   audioencoder_faac.cpp
 * Author: zhujia
 * 
 * Created on 2022年2月6日, 下午8:19
 */

#include "audioencoder_faac.h"
#include<string.h>
#include<stdlib.h>
audioencoder_faac::audioencoder_faac() {
}

audioencoder_faac::audioencoder_faac(const audioencoder_faac& orig) {
}

audioencoder_faac::~audioencoder_faac() {
}

int audioencoder_faac::init(int freq,int channels){
	unsigned long inputSamples=0,maxOutputBytes=0;
	m_hEncoder=faacEncOpen(freq,channels,&inputSamples,&maxOutputBytes);
	m_bufferSize=maxOutputBytes;
	m_outputBuffer=(uint8_t *)malloc(m_bufferSize);
	//2048,8192,左右各1024
	//printf("inputSamples:%lu maxOutputBytes:%lu\n",inputSamples,maxOutputBytes);
	faacEncConfigurationPtr ptr=faacEncGetCurrentConfiguration(m_hEncoder);  
	//ptr->outputFormat=RAW_STREAM;
	ptr->outputFormat=ADTS_STREAM;
	ptr->inputFormat=FAAC_INPUT_FLOAT;
	//ptr->inputFormat=FAAC_INPUT_16BIT;
	ptr->aacObjectType=LOW;
	ptr->mpegVersion=MPEG2;
	faacEncSetConfiguration(m_hEncoder,ptr);
	m_fp=fopen("aaa.aac","wb");
	return 0;
}
int audioencoder_faac::close(){
	faacEncClose(m_hEncoder);
	fclose(m_fp);
	return 0;
}
int audioencoder_faac::get_faac_headinfo(uint8_t*data, int*datalen){
	uint8_t*ppBuffer=NULL;
	faacEncGetDecoderSpecificInfo(m_hEncoder,&ppBuffer,(unsigned long *)datalen);
	memcpy(data,ppBuffer,*datalen);
	return 0;
}
uint8_t*audioencoder_faac::encoder_faac_frame(uint8_t*indata, int inlen, int*outlen){
	//int samplesInput=inlen/4;
	int samplesInput=2048;
	*outlen=faacEncEncode(m_hEncoder,(int32_t *)indata,samplesInput,m_outputBuffer,m_bufferSize);
	if(*outlen>0){
		fwrite(m_outputBuffer,1,*outlen,m_fp);
	}
	return m_outputBuffer;
}