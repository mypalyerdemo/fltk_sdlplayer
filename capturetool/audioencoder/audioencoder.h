/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   audioencoder.h
 * Author: Administrator
 *
 * Created on 2022年1月27日, 上午10:54
 */

#ifndef AUDIOENCODER_H
#define AUDIOENCODER_H
#include<stdint.h>
#include<stdio.h>
class audioencoder {
public:
	audioencoder();
	audioencoder(const audioencoder& orig);
	virtual ~audioencoder();
	static audioencoder*create_audioencoder_bytype(const char*type);
	virtual int init(int freq,int channels)=0;
	virtual int close()=0;
	virtual int get_faac_headinfo(uint8_t*data, int*datalen)=0;
	virtual uint8_t*encoder_faac_frame(uint8_t*indata, int inlen, int*outlen)=0;
private:

};

#endif /* AUDIOENCODER_H */

