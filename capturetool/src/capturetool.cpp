/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "capturetool.h"
#include "bmptool.h"
#include <Windows.h>
#include <SDL2/SDL.h>
capturetool::capturetool(){
	m_fps=25;
}
capturetool::~capturetool(){
}
static unsigned int _sdl_capture_timer (unsigned int interval,void *data){
	capturetool*p=(capturetool*)data;
	p->capture_one_frame();
	return interval;
}
static void _sdl_capture_audio(void *userdata, Uint8 *stream, int len){
    capturetool*p=(capturetool*)userdata;
	p->capture_audio(stream,len);
}
int capturetool::start_recoder(){
	m_filetool=filetool::create_filetool_bytype("flv");
	m_filetool->create("aaa.flv");
	m_videoencoder=videoencoder::create_videoencoder_bytype("x264");
//获取屏幕宽度高度
	HDC hDC=GetDC(NULL);
	int Width = GetDeviceCaps(hDC, HORZRES);
	int Height = GetDeviceCaps(hDC, VERTRES);
//都为4的倍数好处理
	Width=Width/4*4;
	//int BitPerPixel=GetDeviceCaps(hDC,BITSPIXEL);
	ReleaseDC(NULL, hDC);	
	InitDesktoptopBmp24BitData(Width,Height);
	m_height=Height;
	m_width=Width;
	m_datalen=GetBMPWidthBytes(Width,24)*Height;
	m_data=(uint8_t*)malloc(m_datalen);
#if USE_YUV_I420
	m_yuvdatalen=m_height*m_width*3/2;
	m_yuvdata=(uint8_t*)malloc(m_datalen);
	m_videoencoder->init(Width,Height,m_fps,COLORTYPE_I420);
#else	
	m_videoencoder->init(Width,Height,m_fps,COLORTYPE_RGB24);
#endif
	m_frames=0;
	do{
		uint8_t sps [64]={0};
		uint8_t pps [64]={0};
		int spslen=0, ppslen=0;
		m_videoencoder->get_x264_headinfo(sps, &spslen, pps, &ppslen);
		m_filetool->write_x264_head(sps, spslen, pps, ppslen,Width,Height);
	}while(0);
	
	m_starttime=SDL_GetTicks64();
	
	m_timerid=SDL_AddTimer(1000/m_fps,_sdl_capture_timer,(void*)this);
////////////音频初始化	
	m_audioencoder=audioencoder::create_audioencoder_bytype(NULL);
	SDL_AudioSpec have={0};
	SDL_AudioSpec m_wavspec={0};
	m_wavspec.callback=_sdl_capture_audio;
	m_wavspec.freq=44100;
	m_wavspec.samples=1024;
	m_wavspec.channels=2;
	m_wavspec.format=AUDIO_F32;
	//m_wavspec.format=AUDIO_U16;
	m_wavspec.userdata=this;
	m_samples=0;
	do{
		m_wav.format.wFormatTag=3;
		m_wav.format.nChannels=2;
		m_wav.format.nSamplesPerSec=44100;
		m_wav.format.wBitsPerSample=32;
		m_wav.format.nBlockAlign=2*32/8;
		m_wav.format.nAvgBytesPerSec=44100*2*32/8;
		OpenWavFile("aaa.wav",&m_wav,0);
	}while(0);
	m_audioencoder->init(m_wavspec.freq,m_wavspec.channels);
	do{
		uint8_t faachead[32]={0};
		int faadheadlen=0;
		m_audioencoder->get_faac_headinfo(faachead,&faadheadlen);
		if(faadheadlen>0){
			m_filetool->write_faac_head(faachead,faadheadlen);
		}
	}while(0);
	m_audiodev=SDL_OpenAudioDevice(NULL, 1, &m_wavspec, &have, SDL_AUDIO_ALLOW_FORMAT_CHANGE);
	SDL_PauseAudioDevice(m_audiodev,0);
	
	return 0;
}
int capturetool::stop_recoder(){
	SDL_PauseAudioDevice(m_audiodev,1);
	SDL_CloseAudioDevice(m_audiodev);
	SDL_RemoveTimer(m_timerid);
	SDL_Delay(1000);
	CloseWavFile(&m_wav);
//处理剩下的
	do{
		byte*ph264buff = NULL;
		int  h264bufflen = 0;
		ph264buff=m_videoencoder->encoder_x264_frame(NULL,0,&h264bufflen);
		if(h264bufflen>0){
			//m_filetool->write_x264_frame(ph264buff,h264bufflen,m_frames*1000/m_fps);
			m_frames++;
		}else{
			break;
		}
	}while(1);
	m_filetool->close();
	delete m_filetool;
	m_videoencoder->close();
	delete m_videoencoder;
	CleanDesktopBmp24BitData();
	free(m_data);
#if USE_YUV_I420	
	free(m_yuvdata);
#endif	
	return 0;
}
int capturetool::capture_one_frame(){
	byte*ph264buff = NULL;
	int  h264bufflen = 0;
	GetDeskBmp24BitDataEx(m_data,m_width, m_height);
	//SaveBmpFileByRGBData(m_width,m_height,24,m_data);
#if USE_YUV_I420	
	RGB24toI420(m_width, m_height,m_data, m_yuvdata, 0);
	ph264buff=m_videoencoder->encoder_x264_frame(m_yuvdata,m_yuvdatalen,&h264bufflen);
#else
	ph264buff=m_videoencoder->encoder_x264_frame(m_data,m_datalen,&h264bufflen);
#endif	
	if(h264bufflen>0){
		//m_filetool->write_x264_frame(ph264buff,h264bufflen,m_frames*1000/m_fps);
		m_filetool->write_x264_frame(ph264buff,h264bufflen,SDL_GetTicks64()-m_starttime);
		m_frames++;
	}
	return 0;
}

int capturetool::capture_audio(uint8_t* stream,int len){
	int outlen=0;
	WriteWavFile(&m_wav,(char*)stream,len);
	do{
		int num=len/4;
		int i=0;
		float *buf=(float*)stream;
		for(i=0;i<num;i++){
			buf[i]*=32768.0;
		}
	}while(0);
	uint8_t *data=m_audioencoder->encoder_faac_frame(stream,len,&outlen);
	if(outlen>0){
		//m_filetool->write_faac_frame(data,outlen,m_samples*1000/44100);
		m_filetool->write_faac_frame(data,outlen,SDL_GetTicks64()-m_starttime);
		m_samples+=(len/8);
		//printf("%d %d\n",len,m_samples);
	}
	return 0;
}