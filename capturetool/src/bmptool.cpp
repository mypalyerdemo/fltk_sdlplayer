/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include<Windows.h>
#include<stdio.h>
#include<time.h>
#include"capturetool.h"
#if USE_SDL2
#include <SDL2/SDL_image.h>
#endif
int GetBMPWidthBytes(int Width,int Bits);
//上下颠倒
void ChangeRGBDataUpDown(byte*dtsdata,const byte*srcdata,int linelen,int height);
void SaveBmpFile(BITMAPINFO info, byte*data);
void SaveBmpFileBySDL(BITMAPINFO info, byte*data);
void SaveDesktopWindowBmp(){
	HDC hDC=GetDC(NULL);
	int Width = GetDeviceCaps(hDC, HORZRES);
	int Height = GetDeviceCaps(hDC, VERTRES);
	//int BitPerPixel=GetDeviceCaps(hDC,BITSPIXEL);
	//INFO("Bits:%d Width:%d Height:%d",BitPerPixel,Width,Height);
	HBITMAP hbmp=CreateCompatibleBitmap(hDC, Width, Height);//以桌面为上下文创建位图
	HDC memHDC=CreateCompatibleDC(hDC);//创建新设备
	SelectObject(memHDC, hbmp);
	BitBlt(memHDC, 0, 0, Width, Height, hDC, 0, 0, SRCCOPY);//将桌面图像保存到新的绘图设备中
	BITMAP bmp;
	GetObject(hbmp,sizeof(BITMAP),&bmp);//获取位图信息
	int BuffSize=bmp.bmWidthBytes*bmp.bmHeight;
	byte*Buffer=(byte*)malloc(BuffSize);
	//INFO("bmHeight:%d :bmWidth:%d :bmBitsPixel:%d bmWidthBytes:%d", 
	//bmp.bmHeight, bmp.bmWidth, bmp.bmBitsPixel, bmp.bmWidthBytes);
	BITMAPINFO bmpinfo={0};
	bmpinfo.bmiHeader.biBitCount=bmp.bmBitsPixel;
	bmpinfo.bmiHeader.biCompression=BI_RGB;
	bmpinfo.bmiHeader.biHeight=bmp.bmHeight;
	bmpinfo.bmiHeader.biWidth=bmp.bmWidth;
	bmpinfo.bmiHeader.biSizeImage=bmp.bmWidthBytes*bmp.bmHeight;
	bmpinfo.bmiHeader.biPlanes=1;
	bmpinfo.bmiHeader.biSize=sizeof(BITMAPINFOHEADER);
//改为24位色
	bmpinfo.bmiHeader.biBitCount = 24;
	bmpinfo.bmiHeader.biSizeImage = 
	GetBMPWidthBytes(bmpinfo.bmiHeader.biWidth, bmpinfo.bmiHeader.biBitCount)*bmp.bmHeight;

	GetDIBits(memHDC,hbmp,0,Height,Buffer,&bmpinfo,DIB_RGB_COLORS);
	SaveBmpFile(bmpinfo, Buffer);
	SaveBmpFileBySDL(bmpinfo, Buffer);
	free(Buffer);
	DeleteObject(hbmp);//删除创建的设备
	DeleteDC(memHDC);
	ReleaseDC(NULL, hDC);	
}
//获取每行实际宽带
int GetBMPWidthBytes(int Width,int Bits) {
	int WidthBytes = Width * Bits / 8;
	if (WidthBytes % 4 != 0) {
		WidthBytes = (WidthBytes / 4 + 1) * 4;
	}
	return WidthBytes;
}
void ChangeRGBDataUpDown(byte*dtsdata,const byte*srcdata,int linelen,int height){
	int i=0;
	while(i<height){
		int pos1=linelen*i;
		int pos2=linelen*(height-i-1);
		memcpy(dtsdata+pos1,srcdata+pos2,linelen);
		i++;
	}
}
void SaveBmpFileBySDL(BITMAPINFO bmpinfo, byte*data) {
	char filename[256] = { 0 };
	snprintf(filename,256, "%d_%d.jpg", time(NULL),rand());
	int lenelen=GetBMPWidthBytes(bmpinfo.bmiHeader.biWidth, bmpinfo.bmiHeader.biBitCount);
	byte*newdata=(byte*)malloc(lenelen*bmpinfo.bmiHeader.biHeight);
	ChangeRGBDataUpDown(newdata,data,lenelen,bmpinfo.bmiHeader.biHeight);
	//数据颠倒
	SDL_Surface*image=SDL_CreateRGBSurfaceWithFormatFrom(const_cast<uint8_t*>(newdata),
		bmpinfo.bmiHeader.biWidth,bmpinfo.bmiHeader.biHeight,bmpinfo.bmiHeader.biBitCount,
		lenelen,SDL_PIXELFORMAT_BGR24);
	IMG_SaveJPG(image,filename,100);
	SDL_FreeSurface(image);
	free(newdata);
}
void SaveBmpFileByRGBData(int width, int height,int bits, uint8_t*data){
	char filename[256] = { 0 };
	snprintf(filename,256, "%d_%d.jpg", time(NULL),rand());
	int lenelen=GetBMPWidthBytes(width, bits);
	SDL_Surface*image=SDL_CreateRGBSurfaceWithFormatFrom(const_cast<uint8_t*>(data),
		width,height,bits,
		lenelen,SDL_PIXELFORMAT_BGR24);
	IMG_SaveJPG(image,filename,100);
	SDL_FreeSurface(image);
}
void SaveBmpFile(BITMAPINFO info, byte*data) {
	BITMAPFILEHEADER bfh = { 0 };//bmp文件头
	bfh.bfType = MAKEWORD('B', 'M');
	bfh.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFO);//数据偏移
	bfh.bfSize = bfh.bfOffBits + info.bmiHeader.biSizeImage;//文件总大小
	char filename[256] = { 0 };
	snprintf(filename,256, "%d_%d.bmp", time(NULL),rand());
	FILE*fp = fopen(filename, "wb");
	if (fp == NULL)return;
	fwrite(&bfh, 1, sizeof(BITMAPFILEHEADER), fp);
	fwrite(&info,1, sizeof(BITMAPINFO), fp);
	fwrite(data, 1, info.bmiHeader.biSizeImage, fp);
	fclose(fp);
}

static HDC g_HDeskDC = NULL;
static HDC g_MemDC = NULL;
static HBITMAP  g_hBmp = NULL;
static BITMAPINFOHEADER  g_bih = { 0 };
void InitDesktoptopBmp24BitData(int width, int height) {
	g_HDeskDC = GetDC(NULL);//桌面窗口绘图句柄
	g_MemDC = CreateCompatibleDC(g_HDeskDC);//创建新设备
	g_hBmp = CreateCompatibleBitmap(g_HDeskDC, width, height);//以桌面为上下文创建位图
	SelectObject(g_MemDC, g_hBmp);	
	//将桌面图像保存到新的绘图设备中
	//BITMAP bmp;
	//GetObject(hBmp,sizeof(BITMAP),&bmp);//获取位图信息
	g_bih.biBitCount = 24;
	g_bih.biCompression = BI_RGB;
	g_bih.biHeight = height;
	g_bih.biWidth = width;
	g_bih.biSizeImage = GetBMPWidthBytes(g_bih.biWidth, g_bih.biBitCount)*height;
	g_bih.biPlanes = 1;
	g_bih.biSize = sizeof(BITMAPINFOHEADER);
}
void CleanDesktopBmp24BitData() {
	DeleteObject(g_hBmp);//删除创建的设备
	DeleteDC(g_MemDC);
	ReleaseDC(NULL, g_HDeskDC);
}
void GetDeskBmp24BitDataEx(uint8_t*data, int width, int height) {
	BitBlt(g_MemDC, 0, 0, width, height, g_HDeskDC, 0, 0, SRCCOPY);
	GetDIBits(g_MemDC,g_hBmp, 0, height, data, (LPBITMAPINFO)&g_bih, DIB_RGB_COLORS);//RGB数据获取
}

/////////////////////////////RGB2I420Start///////////////////////////////////////////////////////////////
static float RGBYUV02990[256], RGBYUV05870[256], RGBYUV01140[256];
static float RGBYUV01684[256], RGBYUV03316[256];
static float RGBYUV04187[256], RGBYUV00813[256];
void InitRGB24toI420();

void RGB24toI420(int inWidth, int inHeight, uint8_t *bmp, uint8_t*yuv_out, int flip){
	uint8_t *y_out=(uint8_t*)yuv_out;
	uint8_t *u_out=(uint8_t*)yuv_out+inWidth*inHeight;
	uint8_t *v_out=(uint8_t*)yuv_out+inWidth*inHeight*5/4;

	static int init_done = 0;

	long i, j, size;
	unsigned char *r, *g, *b;
	unsigned char *y, *u, *v;
	unsigned char *pu1, *pu2, *pv1, *pv2, *psu, *psv;
	unsigned char *y_buffer, *u_buffer, *v_buffer;
	unsigned char *sub_u_buf, *sub_v_buf;


	int WidthBytes=inWidth*3;
	if (WidthBytes%4!=0){
		WidthBytes=(WidthBytes/4+1)*4;
	}

	if (init_done == 0)	{
		InitRGB24toI420();
		init_done = 1;
	}

// check to see if inWidth and inHeight are divisible by 2
	if ((inWidth % 2) || (inHeight % 2)) return ;
		size = inWidth * inHeight;

// allocate memory
	y_buffer = (unsigned char *)y_out;
	sub_u_buf = (unsigned char *)u_out;
	sub_v_buf = (unsigned char *)v_out;
	u_buffer = (unsigned char *)malloc(size * sizeof(unsigned char));
	v_buffer = (unsigned char *)malloc(size * sizeof(unsigned char));
	if (!(u_buffer && v_buffer))
	{
		if (u_buffer) free(u_buffer);
		if (v_buffer) free(v_buffer);
		return ;
	}

	b = (unsigned char *)bmp;
	y = y_buffer;
	u = u_buffer;
	v = v_buffer;

// convert RGB to YUV
	if (!flip) {
		for (j = 0; j < inHeight; j ++)
		{
			y = y_buffer + (inHeight - j - 1) * inWidth;
			u = u_buffer + (inHeight - j - 1) * inWidth;
			v = v_buffer + (inHeight - j - 1) * inWidth;

			b=(uint8_t*)bmp+WidthBytes*j;

			for (i = 0; i < inWidth; i ++) {
				g = b + 1;
				r = b + 2;
				*y = (uint8_t)( RGBYUV02990[*r] + RGBYUV05870[*g] + RGBYUV01140[*b]);
				*u = (uint8_t)(- RGBYUV01684[*r] - RGBYUV03316[*g] + (*b)/2 + 128);
				*v = (uint8_t)( (*r)/2 - RGBYUV04187[*g] - RGBYUV00813[*b] + 128);
				b += 3;
				y ++;
				u ++;
				v ++;
			}
		}
	} else {
		for (j = 0; j < inHeight; j ++)
		{
			b=(uint8_t*)bmp+WidthBytes*j;
			for (i = 0; i < inWidth; i++)
			{
				g = b + 1;
				r = b + 2;
				*y = (uint8_t)( RGBYUV02990[*r] + RGBYUV05870[*g] + RGBYUV01140[*b]);
				*u = (uint8_t)(- RGBYUV01684[*r] - RGBYUV03316[*g] + (*b)/2 + 128);
				*v = (uint8_t)( (*r)/2 - RGBYUV04187[*g] - RGBYUV00813[*b] + 128);
				b += 3;
				y ++;
				u ++;
				v ++;
			}
		}
	}

// subsample UV
	for (j = 0; j < inHeight/2; j ++)
	{
		psu = sub_u_buf + j * inWidth / 2;
		psv = sub_v_buf + j * inWidth / 2;
		pu1 = u_buffer + 2 * j * inWidth;
		pu2 = u_buffer + (2 * j + 1) * inWidth;
		pv1 = v_buffer + 2 * j * inWidth;
		pv2 = v_buffer + (2 * j + 1) * inWidth;
		for (i = 0; i < inWidth/2; i ++)
		{
			*psu = (*pu1 + *(pu1+1) + *pu2 + *(pu2+1)) / 4;
			*psv = (*pv1 + *(pv1+1) + *pv2 + *(pv2+1)) / 4;
			psu ++;
			psv ++;
			pu1 += 2;
			pu2 += 2;
			pv1 += 2;
			pv2 += 2;
		}
	}
	free(u_buffer);
	free(v_buffer);
//return 0;
}

void InitRGB24toI420(){
	int i;
	for (i = 0; i < 256; i++) RGBYUV02990[i] = (float)0.2990 * i;
	for (i = 0; i < 256; i++) RGBYUV05870[i] = (float)0.5870 * i;
	for (i = 0; i < 256; i++) RGBYUV01140[i] = (float)0.1140 * i;
	for (i = 0; i < 256; i++) RGBYUV01684[i] = (float)0.1684 * i;
	for (i = 0; i < 256; i++) RGBYUV03316[i] = (float)0.3316 * i;
	for (i = 0; i < 256; i++) RGBYUV04187[i] = (float)0.4187 * i;
	for (i = 0; i < 256; i++) RGBYUV00813[i] = (float)0.0813 * i;
}