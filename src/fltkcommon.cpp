/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include"fltkcommon.h"
void fltk_count_autosize(int srcw,int srch,int *dtsw,int *dtsh){
//比较宽高比
	if((1.0*srcw/srch)>(1.0*(*dtsw)/(*dtsh))){
		*dtsh=srch*(*dtsw)/srcw;
	}else{
		*dtsw=srcw*(*dtsh)/srch;
	}
}
void fltk_playtime_format(int playtime,char*playbuff,int buffsize){
	int hour=playtime/3600;
	int min=(playtime-3600*hour)/60;
	int sec=playtime%60;
	snprintf(playbuff,buffsize,"%02d:%02d:%02d",hour,min,sec);
}
void fltk_showtime_format(int playtime,int totaltime,char*playbuff,int buffsize){
	char playtimestr[16]={0};
	char totaltimestr[16]={0};
	fltk_playtime_format(playtime,playtimestr,16);
	fltk_playtime_format(totaltime,totaltimestr,16);
	snprintf(playbuff,buffsize,"%s/%s",playtimestr,totaltimestr);
}