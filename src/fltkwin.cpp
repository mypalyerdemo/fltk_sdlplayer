/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fltkwin.cpp
 * Author: Administrator
 * 
 * Created on 2021年2月12日, 下午1:49
 */

#include"fltkwin.h"
#include"fltkplayer.h"
#include"fltkrecoder.h"
#include"fltkimagetool.h"
#include"fltkdesktopcapture.h"
static void _openrecoder_callback(Fl_Widget *w, void *data) {
	fltkrecoder*fltktool=new fltkrecoder();
	fltktool->init();
}
static void _openplayer_callback(Fl_Widget *w, void *data) {
	fltkplayer*fltktool=new fltkplayer();
	fltktool->init();
}
static void _openimagetool_callback(Fl_Widget *w, void *data) {
	fltkimagetool*fltktool=new fltkimagetool();
	fltktool->init();
}
static void _opendesktopcapturetool_callback(Fl_Widget *w, void *data) {
	fltkdesktopcapture*fltktool=new fltkdesktopcapture();
	fltktool->init();
}
fltkwin::fltkwin()
{
	
}

fltkwin::fltkwin(const fltkwin& orig)
{
}
void fltkwin::init()
{
	m_window = new Fl_Window(640,480,"主界面");
	Fl_Shared_Image*tmpimg=NULL;//Fl_Shared_Image::get("./img/back.gif");	
	if(tmpimg){
		m_window->image(tmpimg);
	}
	//视频播放按钮
	m_playerbutton = new Fl_Button(20, 20, 80, 80, "播放器");
	m_playerbutton->type(FL_NORMAL_BUTTON);
	m_playerbutton->callback(_openplayer_callback,(void*)this);
	
	tmpimg=Fl_Shared_Image::get("./img/video.gif");	
	if(tmpimg){
		m_playerbutton->image(tmpimg);
	}
	
	//录音按钮
	m_recoderbutton = new Fl_Button(120, 20, 80, 80, "录音机");
	m_recoderbutton->type(FL_NORMAL_BUTTON);
	m_recoderbutton->callback(_openrecoder_callback,(void*)this);	
	
	tmpimg=Fl_Shared_Image::get("./img/audio.gif");	
	if(tmpimg){
		m_recoderbutton->image(tmpimg);
	}
	
	m_imagetoolbutton = new Fl_Button(220, 20, 80, 80, "图片");
	m_imagetoolbutton->type(FL_NORMAL_BUTTON);
	m_imagetoolbutton->callback(_openimagetool_callback,(void*)this);	
	
	tmpimg=Fl_Shared_Image::get("./img/image.gif");	
	if(tmpimg){
		m_imagetoolbutton->image(tmpimg);
	}
	
	m_deskcapturebutton=new Fl_Button(320, 20, 80, 80, "桌面录制");
	m_deskcapturebutton->type(FL_NORMAL_BUTTON);
	m_deskcapturebutton->callback(_opendesktopcapturetool_callback,(void*)this);
	
	m_window->end();
	//感觉不加没事
	m_window->show(0, NULL);

}
void fltkwin::uninit(){

}

fltkwin::~fltkwin()
{
	uninit();
}

