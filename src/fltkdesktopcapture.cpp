/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fltkdesktopcapture.cpp
 * Author: Administrator
 * 
 * Created on 2022年1月26日, 下午4:38
 */

#include "fltkdesktopcapture.h"
#include "bmptool.h"
#include "capturetool.h"
static void _startcapture_callback(Fl_Widget *w, void *data) {
	fltkdesktopcapture*p=(fltkdesktopcapture*)data;
	p->start_desktopcapture();
}
static void _stopcapture_callback(Fl_Widget *w, void *data) {
	fltkdesktopcapture*p=(fltkdesktopcapture*)data;
	p->stop_desktopcapture();	
}
typedef struct SDL_VideoDevice SDL_VideoDevice;
#ifdef __cplusplus
extern "C" {
#endif
extern DECLSPEC SDL_VideoDevice * SDLCALL SDL_GetVideoDevice(void);
#ifdef __cplusplus
}
#endif
typedef struct
{
    WCHAR DeviceName[32];
    HMONITOR MonitorHandle;
    SDL_bool IsValid;
} SDL_DisplayData;
static void _savescreen_callback(Fl_Widget *w, void *data) {
	SaveDesktopWindowBmp();
}

fltkdesktopcapture::fltkdesktopcapture()
{
}

fltkdesktopcapture::~fltkdesktopcapture()
{
}

void fltkdesktopcapture::init(){
	m_window = new Fl_Window(800,220,"桌面录制");
	
	m_startbutton = new Fl_Button(510, 10, 80, 40, "Start");
	m_startbutton->type(FL_NORMAL_BUTTON);
	m_startbutton->callback(_startcapture_callback,(void*)this);
	
	m_stopbutton = new Fl_Button(600, 10, 80, 40, "Stop");
	m_stopbutton->type(FL_NORMAL_BUTTON);
	m_stopbutton->callback(_stopcapture_callback,(void*)this);
	
	m_screenshotbutton = new Fl_Button(690, 10, 80, 40, "Screen");
	m_screenshotbutton->type(FL_NORMAL_BUTTON);
	m_screenshotbutton->callback(_savescreen_callback,(void*)this);
	m_window->end();
	m_window->show();
	m_capturetool=NULL;
}
void fltkdesktopcapture::start_desktopcapture(){
	if(m_capturetool==NULL){
		m_capturetool=new capturetool();
		((capturetool*)m_capturetool)->start_recoder();
	}
}
void fltkdesktopcapture::stop_desktopcapture(){
	if(m_capturetool){
		((capturetool*)m_capturetool)->stop_recoder();
		delete (capturetool*)m_capturetool;
		m_capturetool=NULL;
	}
}