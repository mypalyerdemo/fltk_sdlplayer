#include"fltkwin.h"
//这里注意在编译的时候记得加上-lfltk -lfltk_images -ljpeg -lpng命令(在MSYS2上还要加上-mwindows -DWIN32 -lole32 -luuid -lcomctl32)。
//-lmingw32 -mwindows -lSDL2main -lSDL2 -lSDL2_image -Dmain=SDL_main
//mingw-w64-i686-ffms2 
//若音频初始化失败要配置环境变量SDL_AUDIODRIVER=directsound
int main(int argc, char** argv)
{
#if USE_SDL2	
	SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER);
	IMG_Init(0xFF);
//防止没声音	
	if(SDL_getenv("SDL_AUDIODRIVER")==NULL){
		SDL_setenv("SDL_AUDIODRIVER","directsound",0);
	}
#endif	
	FFMS_Init(0,0);
	fl_register_images();
	fltkwin*fltktool=new fltkwin();
	fltktool->init();
	Fl::run();
#if USE_SDL2	
	IMG_Quit();
	SDL_Quit();
#endif	
	return 0;
}

