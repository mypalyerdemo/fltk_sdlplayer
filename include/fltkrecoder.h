/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fltkrecoder.h
 * Author: Administrator
 *
 * Created on 2021年3月5日, 下午3:11
 */

#ifndef FLTKRECODER_H
#define FLTKRECODER_H
#include"fltkcommon.h"
#include<soundtouch/SoundTouch.h>
using namespace soundtouch;
class fltkrecoder {
	Fl_Window *m_window;
	
	Fl_Choice *m_playdevicelist;
	Fl_Choice *m_capturedevicelist;
	Fl_Choice *m_driverlist;
	
	Fl_Choice *m_speedlist;
	Fl_Choice *m_audiotypelist;
	
	Fl_Button *m_reflashbutton;
	Fl_Button *m_recordeaudiobutton;
	Fl_Button *m_stopaudiobutton;
	Fl_Button *m_playaudiobutton;
	
	Fl_File_Input *m_fileinput;
	Fl_Button *m_resetbutton;
	Fl_Button *m_loadwavbutton;
//播放速度	
	float m_speed;
//变声	
	int   m_audiotype;
//播放用变量       
    char*  m_audiobuff;
//缓存大小	
    int    m_audiobuff_buffsize;
//录制用变量
//播放大小，数据位置	
    int    m_audiobuff_playsize;	
//录音大小	
    int    m_audiobuff_recodersize;
//音频信息	
	SDL_AudioSpec m_wavspec;
//设备ID	
	SDL_AudioDeviceID m_audiodev;
//变速
	SoundTouch m_soundTouch;
public:
	fltkrecoder();
	virtual ~fltkrecoder();
	void init();
	void uninit();
	void reflashdevicelist();
//加载wav文件	
	void loadwav_callback();
//开始录音	
	void startrecoder_callback();
//停止录音	
    void stoprecoder_callback();
//播放录音	
    void playrecoder_callback();
//录音回掉	
    void recoderaudiobuff(void *stream,int len);
//播放回掉	
	void playaudiobuff(void *stream,int len);
private:

};

#endif /* FLTKRECODER_H */

