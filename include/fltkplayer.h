/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fltkplayer.h
 * Author: Administrator
 *
 * Created on 2021年3月5日, 下午3:11
 */

#ifndef FLTKPLAYER_H
#define FLTKPLAYER_H
#include"fltkcommon.h"
#include <FL/Fl_Image_Surface.H>
#include <FL/Fl_Overlay_Window.H>
#include <FL/Fl_Hor_Value_Slider.H>
#define USE_SDL_WIN_PLAY 0
class fltkplayer {
	Fl_Double_Window *m_window;
	Fl_File_Input *m_input;
	Fl_Button *m_resetbutton;
	Fl_Button *m_saveimgbutton;
	
	Fl_Button *m_playbutton;
	Fl_Button *m_pausebutton;
	Fl_Button *m_stopbutton;
	
	//Fl_Hor_Value_Slider *m_playprogress;
	Fl_Box* m_imgbox;

	FFMS_VideoSource *m_videosource;
	FFMS_AudioSource *m_audiosource;
	int m_videoframes;
	int m_videopos;
	//int m_videowidth;
	//int m_videoheigh;
	int m_FPSDenominator;
    int m_FPSNumerator;
	float m_totaltime;
    int64_t m_audiosamles;
    int64_t m_audiosamlepos;
//播放用变量       
    char*  m_audiobuff;
    int    m_audiobuffsize;	
	SDL_AudioDeviceID m_audiodev;
        
    SDL_TimerID m_timerid;
#if USE_SDL_WIN_PLAY	
	SDL_Window  *m_sdlwindow;
	SDL_Surface *m_sdlscreen;
#else
	SDL_Surface *m_sdlimage;
#endif	
public:
	fltkplayer();
	fltkplayer(const fltkplayer& orig);
	virtual ~fltkplayer();
	void init();
	void uninit();
//保存视频截图
    void saveimg_callback();
	void play_thread();
	void play_timer();
	void pause_callback();
	void stop_callback();
    void playaudiobuff(void *stream,int len);
private:

};

#endif /* FLTKPLAYER_H */

