/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fltkimagetool.h
 * Author: Administrator
 *
 * Created on 2021年6月19日, 下午8:10
 */

#ifndef FLTKIMAGETOOL_H
#define FLTKIMAGETOOL_H
#include"fltkcommon.h"
class fltkimagetool {
	Fl_Window *m_window;
	
	Fl_File_Input *m_input;
	Fl_Button *m_resetbutton;
	
	Fl_Button *m_openimgbutton;
    Fl_Button *m_saveimgbutton;
	
	Fl_Box* m_imgbox;	
	SDL_Window  *m_sdlwindow;
	SDL_Surface *m_sdlscreen;
public:
	fltkimagetool();
	fltkimagetool(const fltkimagetool& orig);
	virtual ~fltkimagetool();
	void init();
	void uninit();	
//读取图片显示
	void openimg_callback();
//保存视频截图
    void saveimg_callback();
private:

};

#endif /* FLTKIMAGETOOL_H */

