/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fltkcommon.h
 * Author: Administrator
 *
 * Created on 2021年3月5日, 下午3:12
 */

#ifndef FLTKCOMMON_H
#define FLTKCOMMON_H

#define USE_SDL2 1

#define USE_FFMS2 1
#include <FL/Fl.H>
#include <FL/x.H>
//#include <FL/win32.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Gl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_File_Input.H>
#include <FL/Fl_JPEG_Image.H>
#include <FL/Fl_Shared_Image.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Bitmap.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Choice.H>
#if USE_SDL2
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_thread.h>
#endif

#if USE_FFMS2
#include<ffms.h>
#endif
//根据原大小计算合适的宽高
void fltk_count_autosize(int srcw,int srch,int *dtsw,int *dtsh);
//格式化播放时间
void fltk_playtime_format(int playtime,char*playbuff,int buffsize);
void fltk_showtime_format(int playtime,int totaltime,char*playbuff,int buffsize);
#endif /* FLTKCOMMON_H */

