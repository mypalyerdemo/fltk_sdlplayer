/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fltkwin.h
 * Author: Administrator
 *
 * Created on 2021年2月12日, 下午1:49
 */

#ifndef FLTKWIN_H
#define FLTKWIN_H

#include"fltkcommon.h"

class fltkwin {
	Fl_Window *m_window;
	
	Fl_Button *m_playerbutton;
    Fl_Button *m_recoderbutton;     
	Fl_Button *m_imagetoolbutton;
	Fl_Button *m_deskcapturebutton;

public:
	fltkwin();
	fltkwin(const fltkwin& orig);
	virtual ~fltkwin();
	void init();
	void uninit();

private:

};

#endif /* FLTKWIN_H */

