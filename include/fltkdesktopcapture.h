/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   fltkdesktopcapture.h
 * Author: Administrator
 *
 * Created on 2022年1月26日, 下午4:38
 */

#ifndef FLTKDESKTOPCAPTURE_H
#define FLTKDESKTOPCAPTURE_H
#include"fltkcommon.h"
class fltkdesktopcapture {
	Fl_Window *m_window;
	Fl_Button *m_startbutton;
	Fl_Button *m_screenshotbutton;
	Fl_Button *m_stopbutton;
public:
	fltkdesktopcapture();
	virtual ~fltkdesktopcapture();
	void init();
	void start_desktopcapture();
	void stop_desktopcapture();
private:
	void*m_capturetool;
};

#endif /* FLTKDESKTOPCAPTURE_H */

